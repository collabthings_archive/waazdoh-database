<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.waazdoh</groupId>
	<artifactId>waazdoh-database</artifactId>

	<packaging>jar</packaging>
	<version>2.0.0-SNAPSHOT</version>
	<name>org.waazdoh.database</name>
	<url>http://maven.apache.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8
		</project.reporting.outputEncoding>
		<maven.compiler.target>9</maven.compiler.target>
		<maven.compiler.source>9</maven.compiler.source>
	</properties>

	<distributionManagement>
		<repository>
			<id>waazdoh-ssh-repository</id>
			<url>sftp://maven@maven.waazdoh.com/opt/maven/repository</url>
		</repository>
	</distributionManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.6.201602180812</version>
				<executions>
					<!-- Prepares the property pointing to the JaCoCo runtime agent which 
						is passed as VM argument when Maven the Surefire plugin is executed. -->
					<execution>
						<id>pre-unit-test</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
						<configuration>
							<!-- Sets the path to the file which contains the execution data. -->
							<destFile>${project.build.directory}/coverage-reports/jacoco-ut.exec
							</destFile>
							<!-- Sets the name of the property containing the settings for JaCoCo 
								runtime agent. -->
							<propertyName>surefireArgLine</propertyName>
						</configuration>
					</execution>
					<!-- Ensures that the code coverage report for unit tests is created 
						after unit tests have been run. -->
					<execution>
						<id>post-unit-test</id>
						<phase>test</phase>
						<goals>
							<goal>report</goal>
						</goals>
						<configuration>
							<!-- Sets the path to the file which contains the execution data. -->
							<dataFile>${project.build.directory}/coverage-reports/jacoco-ut.exec
							</dataFile>
							<!-- Sets the output directory for the code coverage report. -->
							<outputDirectory>${project.reporting.outputDirectory}/jacoco-ut
							</outputDirectory>
						</configuration>
					</execution>
					<!-- The Executions required by unit tests are omitted. -->
					<!-- Prepares the property pointing to the JaCoCo runtime agent which 
						is passed as VM argument when Maven the Failsafe plugin is executed. -->
					<execution>
						<id>pre-integration-test</id>
						<phase>pre-integration-test</phase>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
						<configuration>
							<!-- Sets the path to the file which contains the execution data. -->
							<destFile>${project.build.directory}/coverage-reports/jacoco-it.exec
							</destFile>
							<!-- Sets the name of the property containing the settings for JaCoCo 
								runtime agent. -->
							<propertyName>failsafeArgLine</propertyName>
						</configuration>
					</execution>
					<!-- Ensures that the code coverage report for integration tests after 
						integration tests have been run. -->
					<execution>
						<id>post-integration-test</id>
						<phase>post-integration-test</phase>
						<goals>
							<goal>report</goal>
						</goals>
						<configuration>
							<!-- Sets the path to the file which contains the execution data. -->
							<dataFile>${project.build.directory}/coverage-reports/jacoco-it.exec
							</dataFile>
							<!-- Sets the output directory for the code coverage report. -->
							<outputDirectory>${project.reporting.outputDirectory}/jacoco-it
							</outputDirectory>
						</configuration>
					</execution>
				</executions>
				<dependencies>
					<dependency>
						<groupId>org.ow2.asm</groupId>
						<artifactId>asm</artifactId>
						<version>[5.0.0,)</version>
					</dependency>
				</dependencies>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.19.1</version>
				<configuration>
					<!-- Sets the VM argument line used when unit tests are run. -->
					<argLine>${surefireArgLine}</argLine>
					<!-- Skips unit tests if the value of skip.unit.tests property is true -->
					<skipTests>${skip.unit.tests}</skipTests>
					<!-- Excludes integration tests when unit tests are run. -->
					<excludes>
						<exclude>**/IT*.java</exclude>
					</excludes>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>2.15</version>
				<executions>
					<!-- Ensures that both integration-test and verify goals of the Failsafe 
						Maven plugin are executed. -->
					<execution>
						<id>integration-tests</id>
						<goals>
							<goal>integration-test</goal>
							<goal>verify</goal>
						</goals>
						<configuration>
							<!-- Sets the VM argument line used when integration tests are run. -->
							<argLine>${failsafeArgLine}</argLine>
							<!-- Skips integration tests if the value of skip.integration.tests 
								property is true -->
							<skipTests>${skip.integration.tests}</skipTests>
						</configuration>
					</execution>
				</executions>
				<dependencies>
					<dependency>
						<groupId>org.ow2.asm</groupId>
						<artifactId>asm</artifactId>
						<version>[5.0.0,)</version>
					</dependency>
				</dependencies>
			</plugin>

			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jdepend-maven-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.10</version>
				<configuration>
					<sourceDirectory>src</sourceDirectory>
					<configLocation>src/main/resources/checkstyle.xml</configLocation>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>2.5.2</version>
				<configuration>
					<findbugsXmlOutput>true</findbugsXmlOutput>
					<findbugsXmlWithMessages>true</findbugsXmlWithMessages>
					<xmlOutput>true</xmlOutput>
				</configuration>
			</plugin>


		</plugins>

		<extensions>
			<extension>
				<groupId>org.apache.maven.wagon</groupId>
				<artifactId>wagon-ssh</artifactId>
				<version>2.6</version>
			</extension>
		</extensions>
	</build>

	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-server</artifactId>
			<version>9.4.11.v20180605</version>
		</dependency>

		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-client</artifactId>
			<version>9.4.11.v20180605</version>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.5.2</version>
		</dependency>

		<dependency>
			<groupId>com.github.ipfs</groupId>
			<artifactId>java-ipfs-api</artifactId>
			<version>v1.2.0</version>
		</dependency>

		<dependency>
			<groupId>com.jcraft</groupId>
			<artifactId>jzlib</artifactId>
			<version>[1.1.2,)</version>
		</dependency>

		<dependency>
			<groupId>org.ow2.asm</groupId>
			<artifactId>asm</artifactId>
			<version>[5.0.0,)</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-io -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-io</artifactId>
			<version>1.3.2</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/redis.clients/jedis -->
		<dependency>
			<groupId>redis.clients</groupId>
			<artifactId>jedis</artifactId>
			<version>2.9.0</version>
			<type>jar</type>
		</dependency>

		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.8.5</version>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.25</version>
		</dependency>

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.2.3</version>
		</dependency>

		<dependency>
			<groupId>org.yaml</groupId>
			<artifactId>snakeyaml</artifactId>
			<version>1.17</version>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-compress</artifactId>
			<version>1.16.1</version>
		</dependency>
	</dependencies>

	<repositories>
		<!-- Repository needed to include the jboss netty library -->
		<repository>
			<id>repository.jboss.org</id>
			<url>http://repository.jboss.org/</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>

		<repository>
			<id>waazdoh</id>
			<name>waazdoh</name>
			<url>https://maven.waazdoh.com/repository</url>
		</repository>

		<repository>
			<id>jitpack.io</id>
			<url>https://jitpack.io</url>
		</repository>
	</repositories>
</project>
