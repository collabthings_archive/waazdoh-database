package waazdoh.database.client;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Client {

	private HttpClient c;
	private String url;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public Client(String url) throws Exception {
		this.url = url;

		c = new HttpClient();
		c.start();
	}

	public String getPublicContent(String name) {
		ContentResponse res;
		try {
			String publicUrl = getPublicUrl(name);
			log.info("getting " + publicUrl);
			res = c.GET(publicUrl);
			return res.getContentAsString();
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			log.error("getPublicContent", e);
			return null;
		}
	}

	private String getPublicUrl(String name) {
		return (this.url + "/public/" + name);
	}
}
