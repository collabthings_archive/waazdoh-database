package waazdoh.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;
import waazdoh.database.server.Start;

public class TestBase extends TestCase {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public static final int PORT = 12544;
	private Start s;

	@Override
	protected void setUp() throws Exception {
		s = new Start(PORT);
		s.start();
	}

	@Override
	protected void tearDown() throws Exception {
		s.stop();
	}

	public int getPort() {
		return PORT;
	}

	public String getUrl() {
		return "http://localhost:" + getPort();
	}
}
