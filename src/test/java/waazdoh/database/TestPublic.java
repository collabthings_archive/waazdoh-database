package waazdoh.database;

import waazdoh.database.client.Client;

public class TestPublic extends TestBase {

	public void testGetReadme() throws Exception {
		Client c = new Client(getUrl());
		String readme = c.getPublicContent("readme.txt");
		log.info("got " + readme);
		assertTrue("# waazdoh.database", readme.contains("# waazdoh.database"));
	}
}
