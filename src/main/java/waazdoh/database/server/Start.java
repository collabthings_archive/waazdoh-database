package waazdoh.database.server;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Start {
	private Server server;
	private int port;
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public Start(int port) {
		this.port = port;
	}

	public void start() throws Exception {
		server = new Server(port);
		ResourceHandler resourcehandler = new ResourceHandler();
		resourcehandler.setDirectoriesListed(true);
		resourcehandler.setResourceBase("public");

		ContextHandler publiccontexthandler = new ContextHandler("/public");
		publiccontexthandler.setHandler(resourcehandler);

		log.info("ResourceBase " + resourcehandler.getResourceBase());

		// Add the ResourceHandler to the server.
		HandlerList handlers = new HandlerList();
		handlers.setHandlers(new Handler[] { publiccontexthandler, new DefaultHandler() });
		server.setHandler(handlers);

		server.start();

	}

	public void stop() throws Exception {
		server.stop();
	}
}
